import random
import math
import player_class
import os.path
from os import path

def main():
    print("Game has started successfully. Welcome to the game!")
    total_score = 0
    total_turn_counter = 1
    turns_until_boss = 10
    is_game_active = True
    my_character = player_class.choose_character()
    print("You have chosen", my_character.get_name(), "as your character!")
    print("His stats are:", vars(my_character))
    while is_game_active == True:
        max_character_health = my_character.get_health()
        if turns_until_boss != 0:
            print("Current round:", total_turn_counter)
            print("Rounds until boss:", turns_until_boss)
            generated_enemy = player_class.generate_enemy(total_turn_counter)
            who_died = player_class.battle_state(generated_enemy, my_character,total_turn_counter)
            player_class.gameOver(who_died, my_character)
            print("XP needed to level up:", player_class.needed_xp_for_level_up(my_character))
            print("Current xp:", my_character.get_xp())
            print("Continue quest? (y)es or (n)o")
            continue_game = input()
            while continue_game!= 'y' and continue_game!= 'n':
                print("Wrong input!")
                print("Continue quest? (y)es or (n)o")
                continue_game = input()
            if continue_game == 'y':
                is_game_active = True
            else:
                is_game_active = False
            total_turn_counter+=1
            turns_until_boss-=1
        else:
            print("Ten turns have passed... Boss incoming!!!")
            generated_boss = player_class.generate_boss(total_turn_counter)
            who_died = player_class.battle_state(generated_boss, my_character,total_turn_counter)
            print("XP needed to level up:", player_class.needed_xp_for_level_up(my_character))
            print("Current xp:", my_character.get_xp())
            player_class.gameOver(who_died, my_character)
            print("Continue quest? (y)es or (n)o")
            continue_game = input()
            while continue_game!= 'y' and continue_game!= 'n':
                print("Wrong input!")
                print("Continue quest? (y)es or (n)o")
                continue_game = input()
            if continue_game == 'y':
                is_game_active = True
            else:
                is_game_active = False
            total_turn_counter+=1
            turns_until_boss-=1
            total_turn_counter+=1
            turns_until_boss=10
    if path.exists("final_score.txt"):
        name_text = repr(my_character.get_name())
        level_text = repr(my_character.get_level())
        f = open("final_score.txt", "a")
        f.write("\n" + name_text + " " + "Level: " + level_text)
        f.close()
    else:
        name_text = repr(my_character.get_name())
        level_text = repr(my_character.get_level())
        f = open("final_score.txt", "x")
        f.write(name_text + " " + "Level: " + level_text)
        f.close()
    print("Thank you for playing")
main()