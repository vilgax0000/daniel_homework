import random
import math
import os.path
from os import path

# player class
class Player:
    def __init__(self):
        pass
    def get_name(self):
        return self.name
    def get_health(self):
        return self.health
    def get_defense(self):
        return self.defense
    def get_attack(self):
        return self.attack
    def get_stamina(self):
        return self.stamina
    def get_special_attack_name(self):
        return self.player_special_name
    def get_special_attack(self):
        return self.player_special
    def get_xp(self):
        return self.player_xp
    def get_level(self):
        return self.player_level
    def get_player_special_cost(self):
        return self.player_special_cost
    def set_name(self, new_name):
        self.name = new_name
    def set_health(self, new_health):
        self.health = new_health
    def set_defense(self, new_defense):
        self.defense = new_defense
    def set_attack(self, new_attack):
        self.attack = new_attack
    def set_stamina(self, new_stamina):
        self.stamina = new_stamina
    def set_special_attack(self, new_player_special):
        self.player_special = new_player_special
    def set_xp(self, new_xp):
        self.player_xp = new_xp
    def set_level(self, new_level):
        self.player_level = new_level
    def set_player_special_cost(self, new_special_cost):
        self.player_special_cost = new_special_cost
    def get_healing_ability(self):
        return self.heal
    def set_healing_ability(self, new_heal):
        self.heal = new_heal
    def get_healing_ability_cost(self):
        return self.heal_cost
    def set_healing_ability_cost(self, new_heal):
        self.heal_cost = new_heal
    
    
class Warrior(Player):
    def __init__(self):
        self.player_level = 1
        self.player_xp = 0
        self.name = 'Ichiban'
        self.health = 93
        self.defense = 39
        self.attack = 22
        self.stamina = 79
        self.player_special_name = "Essence of Bonecrushing Bat"
        self.player_special = math.ceil(self.get_attack()*1.8)
        self.player_special_cost = 29
        self.heal = 30
        self.heal_cost = 30

class Tank(Player):
    def __init__(self):
        self.player_level = 1
        self.player_xp = 0
        self.name = 'Adachi'
        self.health = 159
        self.defense = 57
        self.attack = 16
        self.stamina = 51
        self.player_special_name = "Reckless Charge"
        self.player_special = math.ceil(self.get_attack()*1.9)
        self.player_special_cost = 32
        self.heal = 42
        self.heal_cost = 30
        
class Mage(Player):
    def __init__(self):
        self.player_level = 1
        self.player_xp = 0
        self.name = 'Nanba'
        self.health = 62
        self.defense = 31
        self.attack = 20
        self.stamina = 85
        self.player_special_name = "Pyro Breath"
        self.player_special = math.ceil(self.get_attack()*2.2)
        self.player_special_cost = 44
        self.heal = 45
        self.heal_cost = 30
        
class Legend(Player):
    def __init__(self):
        self.player_level = 1
        self.player_xp = 0
        self.name = 'Kiryu'
        self.health = 250
        self.defense = 120
        self.attack = 50
        self.stamina = 150
        self.player_special_name = "Essence of the Dragon"
        self.player_special = math.ceil(self.get_attack()*2.7)
        self.player_special_cost = 90
        self.heal = 55
        self.heal_cost = 30

#enemy class       
class Enemy(Player):
    def __init__(self, enemy_health, enemy_attack, enemy_special_attack, enemy_chance, enemy_name):
        self.health=enemy_health
        self.attack=enemy_attack
        self.special_attack = enemy_special_attack
        self.chance=enemy_chance
        self.name=enemy_name
    def get_name(self):
        return self.name
    def get_health(self):
        return self.health
    def get_defense(self):
        return self.defense
    def get_attack(self):
        return self.attack
    def get_stamina(self):
        return self.stamina
    def get_special(self):
        return self.special_attack
    def get_chance(self):
        return self.chance
    def set_name(self, new_name):
        self.name = new_name
    def set_health(self, new_health):
        self.health = new_health
    def set_defense(self, new_defense):
        self.defense = new_defense
    def set_attack(self, new_attack):
        self.attack = new_attack
    def set_stamina(self, new_stamina):
        self.stamina = new_stamina
    def set_special(self, new_special):
        self.special_attack=new_special
    def set_chance(self, new_chance):
        self.chance=new_chance

#boss class
class Boss_Enemy(Enemy):
    def __init__(self, enemy_health, enemy_attack, enemy_special_attack, enemy_chance, enemy_name, boss_special):
        super().__init__(enemy_health, enemy_attack, enemy_special_attack, enemy_chance, enemy_name)
        self.boss_special=boss_special
    
    def get_boss_special(self):
        return self.boss_special
    def set_boss_special(self, new_boss_special):
        self.boss_special=new_boss_special

#Function to generate a new enemy
#Explanation 1
def generate_enemy(turn_count):
    exponent = 0.3
    file = open("enemies.txt", 'r')
    lines = file.readlines()
    generated_name = lines[random.randint(0,len(lines)-1)][:-1]
    file.close
    enemy_health = math.ceil(random.randint(28,42)*(turn_count**exponent))
    enemy_attack = math.ceil(random.randint(4,9)*(turn_count**exponent))
    enemy_special_attack = math.ceil(random.randint(12,19)*(turn_count**exponent))
    enemy_chance = math.ceil(random.randint(1,10)*(turn_count**exponent))
    enemy_name=generated_name
    return Enemy(enemy_health, enemy_attack, enemy_special_attack, enemy_chance, enemy_name)

#generate boss
#Explanation 2
def generate_boss(turn_count):
    exponent = 0.4
    file = open("boss names.txt", 'r')
    lines = file.readlines()
    generated_name = lines[random.randint(0,len(lines)-1)][:-1]
    file.close
    enemy_health = math.ceil(random.randint(49,62)*(turn_count**exponent))
    enemy_attack = math.ceil(random.randint(19,25)*(turn_count**exponent))
    enemy_special_attack = math.ceil(random.randint(34,41)*(turn_count**exponent))
    enemy_chance = math.ceil(random.randint(1,10)*(turn_count**exponent))
    boss_special = math.ceil(random.randint(33,40)*(turn_count**exponent))
    enemy_name=generated_name
    return Boss_Enemy(enemy_health, enemy_attack, enemy_special_attack, enemy_chance, enemy_name, boss_special)

#function for the enemy when it attacks
#Explanation 3   
def func_enemy_attack(enemy_chance, attack_value, enemy_name, defense):
    #uses league of legends armor formula: m = 100/(100+armor); defense = armor
    multiplier = 100/(100+defense)
    print(enemy_name, "is preparing to attack the player!")
    hit_chance = random.randint(0,4)
    if enemy_chance >= hit_chance:
        print("The attack has been successful")
        hp_loss = attack_value * multiplier
        print("You stagger losing...", math.ceil(hp_loss), "health")
        return math.ceil(hp_loss)
    else:
        print("The enemy has missed!")
        return 0

#function for the enemy when it attacks, but it takes into account if the player has blocked
#Explanation 4
def func_enemy_attack_blocked(enemy_chance, attack_value, enemy_name, defense, health):
    #uses league of legends armor formula: m = 100/(100+armor); defense = armor
    total = defense + health
    multiplier = 100/(100+total)
    print(enemy_name, "is preparing to attack the player!")
    hit_chance = random.randint(0,4)
    if enemy_chance >= hit_chance:
        print("The attack has been successful")
        hp_loss = attack_value * multiplier
        print("You stagger losing...", math.ceil(hp_loss), "health")
        return math.ceil(hp_loss)
    else:
        print("The enemy has missed!")
        return 0

#same as above, but for the special attack
def func_enemy_special_attack(enemy_chance, attack_value, enemy_name, defense):
    #uses league of legends armor formula: m = 100/(100+armor); defense = armor
    multiplier = 100/(100+defense)
    print(enemy_name, "is preparing to attack the player!")
    hit_chance = random.randint(0,4)
    if enemy_chance >= hit_chance:
        print("The attack has been successful")
        hp_loss = (attack_value * 1.8) * multiplier
        print("You stagger losing...", math.ceil(hp_loss), "health")
        return math.ceil(hp_loss)
    else:
        print("The enemy has missed!")
        return 0

#same as above, but for the special attack (block included)
def func_enemy_special_attack_blocked(enemy_chance, attack_value, enemy_name, defense, health):
    #uses league of legends armor formula: m = 100/(100+armor); defense = armor
    total = defense + health
    multiplier = 100/(100+total)
    print(enemy_name, "is preparing to attack the player!")
    hit_chance = random.randint(0,4)
    if enemy_chance >= hit_chance:
        print("The attack has been successful")
        hp_loss = (attack_value * 1.8) * multiplier
        print("You stagger losing...", math.ceil(hp_loss), "health")
        return math.ceil(hp_loss)
    else:
        print("The enemy has missed!")
        return 0

#checks if the player or enemy has died. Returns a bool
def is_dead(health):
    if health < 1:
        return True
    else:
        return False

#checks if enemy is dead. Returns a bool
def gameOver(enemyDead, player):
    if enemyDead == True:
        print("\n")
    else:
        print("You are out health")
        print("Better luck next time!!")
        save_score(player)
        exit()

def save_score(player):
    if path.exists("final_score.txt"):
        name_text = repr(player.get_name())
        level_text = repr(player.get_level())
        f = open("final_score.txt", "a")
        f.write("\n" + name_text + " " + "Level: " + level_text)
        f.close()
    else:
        name_text = repr(player.get_name())
        level_text = repr(player.get_level())
        f = open("final_score.txt", "x")
        f.write(name_text + " " + "Level: " + level_text)
        f.close()

#Generate a random xp value       
def generate_xp(turn_count):
    exponent = 0.4
    base_xp = random.randint(6,9)
    our_xp = math.floor(base_xp * (turn_count ** exponent))
    return our_xp

#Generate a random xp value, but by defeating a boss
def generate_boss_xp(turn_count):
    base_xp = random.randint(18,22)
    exponent = 0.7
    our_xp = math.floor(base_xp * (turn_count ** exponent))
    return our_xp

#Calculates the xp neccessary to level up    
def needed_xp_for_level_up(our_character):
    level = our_character.get_level()
    exponent = 1.2
    baseXP = 20
    return math.floor(baseXP * (level ** exponent))

def player_level_up(our_character):
    current_level = our_character.get_level()
    result_warrior = isinstance(our_character, Warrior)
    if result_warrior == True:
        #balanced exponent
        exponent = 0.4
        base_health = 93
        base_defense = 39
        base_attack = 22
        base_stamina = 79
        base_heal = 30
        base_heal_cost = 30
        new_health = math.floor(base_health * (our_character.get_level() ** exponent))
        new_defense = math.floor(base_defense * (our_character.get_level() ** exponent))
        new_attack = math.floor(base_attack * (our_character.get_level() ** exponent))
        new_stamina = math.floor(base_stamina * (our_character.get_level() ** exponent))
        new_heal = math.floor(base_heal * (our_character.get_level() ** exponent))
        new_heal_cost = math.floor(base_heal_cost * (our_character.get_level() ** exponent))
        our_character.set_health(new_health)
        our_character.set_defense(new_defense)
        our_character.set_attack(new_attack)
        our_character.set_stamina(new_stamina)   
        our_character.set_healing_ability(new_heal)   
    result_tank = isinstance(our_character, Tank)
    if result_tank == True:
        base_health = 159
        base_defense = 57
        base_attack = 16
        base_stamina = 51
        base_heal = 42
        base_heal_cost = 30
        #bigger exponent on health, stamina and defense
        health_stamina_defense_exponent = 0.6
        exponent = 0.2
        new_health = math.floor(base_health * (our_character.get_level() ** health_stamina_defense_exponent))
        new_defense = math.floor(base_defense * (our_character.get_level() ** health_stamina_defense_exponent))
        new_attack = math.floor(base_attack * (our_character.get_level() ** exponent))
        new_stamina = math.floor(base_stamina * (our_character.get_level() ** health_stamina_defense_exponent))
        new_heal = math.floor(base_heal * (our_character.get_level() ** exponent))
        new_heal_cost = math.floor(base_heal_cost * (our_character.get_level() ** exponent))
        our_character.set_health(new_health)
        our_character.set_defense(new_defense)
        our_character.set_attack(new_attack)
        our_character.set_stamina(new_stamina)
        our_character.set_healing_ability(new_heal)
    result_mage = isinstance(our_character, Mage)
    if result_mage == True:
        base_health = 62
        base_defense = 31
        base_attack = 20
        base_stamina = 85
        base_heal = 45
        base_heal_cost = 30
        #bigger exponent on attack
        attack_exponent = 0.7
        exponent = 0.3
        new_health = math.floor(base_health * (our_character.get_level() ** exponent))
        new_defense = math.floor(base_defense * (our_character.get_level() ** exponent))
        new_attack = math.floor(base_attack * (our_character.get_level() ** attack_exponent))
        new_stamina = math.floor(base_stamina * (our_character.get_level() ** exponent))
        new_heal = math.floor(base_heal * (our_character.get_level() ** exponent))
        new_heal_cost = math.floor(base_heal_cost * (our_character.get_level() ** exponent))
        our_character.set_health(new_health)
        our_character.set_defense(new_defense)
        our_character.set_attack(new_attack)
        our_character.set_stamina(new_stamina)
        our_character.set_healing_ability(new_heal)
    result_legend = isinstance(our_character, Legend)
    if result_legend == True:
        base_health = 250
        base_defense = 120
        base_attack = 50
        base_stamina = 150
        base_heal = 55
        base_heal_cost = 30
        #max exponent
        exponent = 1.2
        new_health = math.floor(base_health * (our_character.get_level() ** exponent))
        new_defense = math.floor(base_defense * (our_character.get_level() ** exponent))
        new_attack = math.floor(base_attack * (our_character.get_level() ** exponent))
        new_stamina = math.floor(base_stamina * (our_character.get_level() ** exponent))
        new_heal = math.floor(base_heal * (our_character.get_level() ** exponent))
        new_heal_cost = math.floor(base_heal_cost * (our_character.get_level() ** exponent))
        our_character.set_health(new_health)
        our_character.set_defense(new_defense)
        our_character.set_attack(new_attack)
        our_character.set_stamina(new_stamina)
        our_character.set_healing_ability(new_heal)
#Our battle function.
#Explanation 5
def battle_state(get_enemy, get_character, turn_count):
    print(get_enemy.get_name(), "has appeared! Stay on your toes!")
    print("His stats are: \n", vars(get_enemy))
    is_boss = isinstance(get_enemy, Boss_Enemy)
    enemy_max_health = get_enemy.get_health()
    #heal_cost = 30
    is_battle = True
    while is_battle == True:
        #Our battle instructions
        print("(a)ttack; (s)pecial attack; (b)lock; (h)eal")
        our_choice = input()
        
        while our_choice!= "a" and our_choice!= "s" and our_choice!= "b" and our_choice!= "h":
            print("Wrong input!")
            print("(a)ttack; (s)pecial attack; (b)lock; (h)eal")
            our_choice = input()
        
        if our_choice == "a":
            our_damage = get_character.get_attack()
        elif our_choice == "s":
            if get_character.get_stamina() >= get_character.get_player_special_cost():
                get_character.set_stamina(get_character.get_stamina()-get_character.get_player_special_cost())
                print("Current stamina:", get_character.get_stamina())
                our_damage = get_character.get_special_attack()
            else:
                print("You do not have enough stamina")
                print("You will now block the next attack!")
                our_choice = 'b'
        elif our_choice == "b":
            our_damage = 0
        else:
            our_damage = 0
        if our_choice == 'h':
            if get_character.get_stamina() >= get_character.get_healing_ability_cost():
                get_character.set_stamina(get_character.get_stamina()-get_character.get_healing_ability_cost())
                print("Current stamina:", get_character.get_stamina())
                print("You have healed for", get_character.get_healing_ability(), "health")
                get_character.set_health(get_character.get_health()+get_character.get_healing_ability())
            else:
                print("You do not have enough stamina")
                print("You will now block the next attack!")
                our_choice = 'b'
                our_damage = 0
                
        get_enemy.set_health(get_enemy.get_health()-our_damage)
        if our_choice == 's':
            print("You used", get_character.get_special_attack_name())
        print("You have attacked", get_enemy.get_name(), "successfully")
        print("You have inflicted", our_damage, "damage")
        print(get_enemy.get_name(), "health has been lowered to", get_enemy.get_health())
        
        # Check if enemy health is below 1
        enemy_dead = is_dead(get_enemy.get_health())
        
        if enemy_dead == False:
            if our_choice != 'b':
                if get_enemy.get_health() <= enemy_max_health/2:
                    print("Enemy will use special attack!")
                    get_character.set_health(get_character.get_health()-
                    func_enemy_special_attack(get_enemy.get_chance(),
                    get_enemy.get_attack(),
                    get_enemy.get_name(),
                    get_character.get_defense()))
                else:
                    get_character.set_health(get_character.get_health()-
                    func_enemy_attack(get_enemy.get_chance(),
                    get_enemy.get_attack(),
                    get_enemy.get_name(),
                    get_character.get_defense()))
            else:
                if get_enemy.get_health() <= enemy_max_health/2:
                    print("Enemy will use special attack!")
                    get_character.set_health(get_character.get_health()-
                    func_enemy_special_attack_blocked(get_enemy.get_chance(),
                    get_enemy.get_attack(),
                    get_enemy.get_name(),
                    get_character.get_defense(),
                    get_character.get_health()))
                else:
                    get_character.set_health(get_character.get_health()-
                    func_enemy_attack_blocked(get_enemy.get_chance(),
                    get_enemy.get_attack(),
                    get_enemy.get_name(),
                    get_character.get_defense(),
                    get_character.get_health()))
            is_character_dead = is_dead(get_character.get_health())
            if is_character_dead == True:
                is_battle = False
                return False
            else:
                print("You have", get_character.get_health(), "health remaining")
        else:
            if is_boss == False:
                xp_obtained = generate_xp(turn_count)
                get_character.set_xp(get_character.get_xp()+xp_obtained)
                if get_character.get_xp() >= needed_xp_for_level_up(get_character):
                    get_character.set_level(get_character.get_level()+1)
                    print("You have leveled up! Congratulations!")
                    print("You are now level", get_character.get_level())
                    player_level_up(get_character)
                    print("Your new stats:", vars(get_character))
            else:
                xp_obtained = generate_boss_xp(turn_count)
                get_character.set_xp(get_character.get_xp()+xp_obtained)
                if get_character.get_xp() >= needed_xp_for_level_up(get_character):
                    get_character.set_level(get_character.get_level()+1)
                    print("You have leveled up! Congratulations!")
                    print("You are now level", get_character.get_level())
                    player_level_up(get_character)
                    print("Your new stats:", vars(get_character))
            is_battle = False
            print("You have defeated the enemy!")
            return True
            
#Function, which allows us to choose our character   
def choose_character():
    selection = False
    while not selection:
        my_input = input("Please, select your character. (1)Warrior, (2)Tank, (3)Mage, (4)Legend...")
        print()
        if my_input == "1":
            return Warrior()
            selection = True
        elif my_input == "2":
            return Tank()
            selection = True
        elif my_input == "3":
            return Mage()
            selection = True
        elif my_input == "4":
            return Legend()
            selection = True
        else:
            print("Incorrect input")
            continue