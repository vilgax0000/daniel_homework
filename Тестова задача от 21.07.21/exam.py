# Group 1
def lens(listoflists):
  return [len(x) for x in listoflists]

def numbers_to_message(pressed_sequence):

    nokia_buttons = {
        -1: " ",
        0: " ",
        1: " ",
        2: ['a', 'b', 'c'],
        3: ['d', 'e', 'f'],
        4: ['g', 'h', 'i'],
        5: ['j', 'k', 'l'],
        6: ['m', 'n', 'o'],
        7: ['p', 'q', 'r', 's'],
        8: ['t', 'u', 'v'],
        9: ['w', 'x', 'y', 'z']
    }

    # nokia_buttons[2][0] == 'a'
    # nokia_buttons[2][1] == 'b'
    # nokia_buttons[2][2] == 'c'

    print(pressed_sequence)
    seq_group = []
    seq_sub_group = []
    for element in pressed_sequence:
        if element in seq_sub_group or seq_sub_group == []:
            seq_sub_group.append(element)
        else:
            seq_group.append(seq_sub_group)
            seq_sub_group = [element]
    seq_group.append(seq_sub_group)
    print(seq_group)

    list_of_values = list(nokia_buttons.values())
    #print(list_of_values)

    current_seq_sub_list_range = lens(seq_group)
    print(current_seq_sub_list_range) 
    # Колко пъти трябва да цикли списъка на ключа, 
    # който съответсва на текущия елемент

    output_list = []
    output_string = ""

    for digit in pressed_sequence:
        if digit in nokia_buttons.keys():
            output_string+=str(nokia_buttons[digit][0])
    print(output_string)
print(numbers_to_message([2, 3, 2, 2, 5, 2, 2, 2])) #= "abc"