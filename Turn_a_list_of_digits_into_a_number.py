digits = [6,6,7,2,7,2,8]
def to_number(list_of_digits):
    number_of_digits = 0
    for digit in list_of_digits: # Обхожда списъка, като digit играе ролята на i
        number_of_digits = number_of_digits * 10 # Умножава общата сума по 10, тоест в първоначално състояние 0 = 0 * 10 = 0
        number_of_digits = number_of_digits + digit # Събира сумата с цифрата в нашият масив 0 = 0 + n_0 (n_0 е първото число )
    return number_of_digits #
print(to_number(digits)) #
print(to_number(digits)/255) # Делим числото, за да разберем дали имаме число