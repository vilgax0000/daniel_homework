def is_palindrome(number):
    num_list = []
    first_half_list = []
    second_half_list = []
    while number % 10:
        digit = number % 10
        num_list.append(digit) # Вкарва единичните цифри в нашият масив - list_of_digits
        number = number // 10
    list_length = len(num_list)
    middle = float(len(num_list))/2
    for i in range(list_length):
        if i < middle:
            first_half_list.append(num_list[i]);
        else:
            second_half_list.append(num_list[i]);
    first_half_list.sort()
    second_half_list.sort()
    if first_half_list==second_half_list:
        return True
    else:
        return False
print(is_palindrome(423545))