def slope_style_score(scores):
    final_score=0
    max_value=0
    min_value=scores[0]
    for i in range(len(scores)):
        if scores[i]>max_value:
            max_value=scores[i]
        else:
            min_value=scores[i]
    scores.remove(max_value)
    scores.remove(min_value)
    for i in range(len(scores)):
        final_score+=scores[i]
    final_score = final_score/len(scores)
    final_score = str(round(final_score, 2))
    return final_score
print(slope_style_score([95, 85.5, 92.5, 71.5, 22.5]))