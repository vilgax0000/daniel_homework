def contains_digits(number, list_of_digits):
    digit_list = []
    while number % 10:
        digit = number % 10
        digit_list.append(digit)
        number = number // 10
    digit_list.sort()
    list_of_digits.sort()
    if (digit_list == list_of_digits):
        return True
    else:
        return False
print(contains_digits(7762257,[2,2,5,7,6,7,7]))