def sum_of_min_and_max(array=[]):
    #implementation
    max_element = 0
    min_element = array[0] #Задава минимална стойност първият елемент от списъка
    for i in range(len(array)): #Обхожда списъка
        if max_element < array[i]: #Ако max_element е по-малко от сегашният елемент от масива
            max_element = array[i] #Му се прислоява текущата стойност на масива
        elif array[i] < min_element: #Сравнява текущ елемент от масива с първият array[0]
            min_element = array[i] #И ако array[i] < min_element, тогава min става текущ елемент на масива
    return max_element + min_element
array = [12,330,-10,7,9,24,1]
print(sum_of_min_and_max(array))