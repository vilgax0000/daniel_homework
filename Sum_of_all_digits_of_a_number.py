#Sum of all digits of a number
n = 3623
def sum_of_digits(n): # Фунцкия за събиране на отделните цифри на нашето число
    sum_of_total_digits = 0
    while n % 10: # Действието се изпълнява докато цифрата е по-голяма от нула
        digit = n%10 # Обект digit, който представлява остатъка
        sum_of_total_digits =  sum_of_total_digits + digit # Прибавя остатъка към нашата сума
        n = n//10 # Разделя числото БЕЗ остатък. Така можем да премахваме последната цифра.
    return sum_of_total_digits # Връща стойността
print(sum_of_digits(n)) # Събира цифрите на нашето число