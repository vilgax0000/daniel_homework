def fib_number(number): # n0 = 0, n1 = 1
    fib_digits_array = []
    previous_number = 0
    current_number = 1
    next_number = 0
    number_of_digits = 0
    for iterator in range(1,15):
        fib_digits_array.append(previous_number)
        next_number = current_number + previous_number
        previous_number = current_number
        current_number = next_number
    for digit in fib_digits_array:
            number_of_digits = number_of_digits * 10
            number_of_digits = number_of_digits + digit
    return number_of_digits
print(fib_number(5))