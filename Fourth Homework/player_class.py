class Player:
    def __init__(self, name, health, list_of_weapons):
        self._name = name
        self._health = health
        self._list_of_weapons = list_of_weapons
    def shoot(self, current_weapon, enemy_player):
        current_damage = current_weapon.weapon_fire()
        enemy_player.take_damage(current_damage)
    def reload(self, current_weapon):
        current_weapon.reload()
    def take_damage(self, damage):
        if self.check_if_player_is_alive():
            self.set_health(self.get_health()-damage)
        if not self.check_if_player_is_alive():
            print("Player is dead!")
    def check_if_player_is_alive(self):
        if self.get_health() > 0:
            return True
        else:
            return False
    def get_name(self):
        return self._name
    def get_health(self):
        return self._health
    def get_list_of_weapons(self):
        return self._list_of_weapons.weapon_name
    def set_name(self, new_name):
        self._name = new_name
    def set_health(self, new_health):
        self._health = new_health
    def set_list_of_weapons(self, new_list_of_weapons):
        self._list_of_weapons = new_list_of_weapons
class Armored_Player(Player):
    def __init__(self, name, health, list_of_weapons, armor):
        super().__init__(name, health, list_of_weapons)
        self._armor = armor
    def get_armor(self):
        return self._armor
    def set_armor(self, new_armor_value):
        self._armor = new_armor_value
    def take_damage(self, damage):
        divide_damage = damage/2 # Our armor takes half the damage
        self.set_health(self.get_health()-divide_damage)
        self.set_armor(self.get_armor()-divide_damage)
class Weapon:
    def __init__(self, weapon_name, damage, clip_size, remaining_bullets):
        self._weapon_name = weapon_name
        self._damage = damage # Поражанието, което нашето оръжие причинява
        self._clip_size = clip_size # Размер на нашият пълнител
        self._remaining_bullets = remaining_bullets # Текущо налични куршуми. Hardcore
    def reload(self):
        self._remaining_bullets = self.clip_size
    def weapon_fire(self):
        if self._remaining_bullets >=0:
            self._remaining_bullets -= 1
            return self._damage
        else:
            print("You have zero bullets remaining. Please reload your weapon!")
            return 0
    def get_weapon_name(self):
        return self._weapon_name
    def get_damage(self):
        return self._damage
    def get_clip_size(self):
        return self._clip_size
    def get_remaining_bullets(self):
        return self._remaining_bullets
    def set_damage(self, new_damage_value):
        self._damage = new_damage_value
    def set_clip_size(self, new_clip_size):
        self._clip_size = new_clip_size
    def set_remaining_bullets(self, new_remaining_bullets):
        self._remaining_bullets = new_remaining_bullets
m4a1 = Weapon("M4A1", 21.5, 25, 15)
ak47 = Weapon("AK-47", 23.5, 30, 20)
mp40 = Weapon("MP-40", 19.5, 40, 25)
ballistic_knife = Weapon("Ballistic knife", 85, 8, 2)
aug = Weapon("AUG", 26.5, 30, 15)
counter_terrorist = Player("SAS", 100, [m4a1, mp40, aug])
terroist = Player("Arctic avanger", 100, [ak47, ballistic_knife])
armored_terroist = Armored_Player("Assault commander", 150, [ak47, ballistic_knife], 100)

#counter_terrorist.shoot(m4a1, armored_terroist)
#print("Health of", armored_terroist.get_name(), " is:", armored_terroist.get_health())
#print("Armor of", armored_terroist.get_name(), " is:", armored_terroist.get_armor())
#terroist.shoot(ballistic_knife, counter_terrorist)
#print("Health of", counter_terrorist.get_name(), " is:", counter_terrorist.get_health())
terroist.shoot(ak47, counter_terrorist)
terroist.shoot(ak47, counter_terrorist)
terroist.shoot(ak47, counter_terrorist)
terroist.shoot(ak47, counter_terrorist)
terroist.shoot(ak47, counter_terrorist)
print("Health of", counter_terrorist.get_name(), " is:", counter_terrorist.get_health())